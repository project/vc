## Data Model

### VC

After some discussion, there is a plan to convert VC to a Drupal module pack, 
which will be namespaced vc_base. The profile will use vc_base, and also 
support default themes for VC (bootstrap). This will permit backwards
compatibility with the original profile setup.

VC will be a module pack, with the base module containing an admin area, and a 
set of functionality modules.Modules are first divided by entity type, as VC 
has multiple entity types. After this, modules may contain sub-modules based on 
that entity type (i.e. vc_contact -> vc_volunteer_contact).

The plan for the base module set is to be an MVP for creating a datbase of 
volunteers that may commit to needs, and report actions on them. In addition, 
basic support for groups and operations. Everything else will be a separate 
contrib module.

The following modules will be included in vc_base. Note the suggested namespace 
on entity-providing modules for your contrib modules

* vc_actions (vc_actions_* or vc_act_*)
  * vc_actions_general
  * vc_actions_volunteer
* vc_contacts (vc_contacts_* or vc_c_*)
  * vc_contacts_volunteers
* vc_dashboard
* vc_events
* vc_group_content (vc_gc_*)
  * vc_gc_contacts
  * vc_gc_resources
* vc_groups (vc_groups_* or vc_g_*)
  * vc_groups_dashboard
  * vc_groups_operations
* vc_needs (vc_needs_* or vc_n_*)
* vc_resources (vc_resources_* or vc_r_*)

_Note: vc_operations is DEPRECATED, will be removed in the future (upon first 
full release), and replaced with vc_groups_operations_

### VC Actions

VCAction entities are often used on dashboards for various "quick action" 
purposes. They are also used for reporting. Despite the name, VCActions do not 
necessarily "act," but are instead records of actions from other entities.

### VC Contacts

VCContact entities have no special fields or handling. They are used to create 
lists of contacts.

The Volunteer Contact (vc_contacts_volunteers) is used by other modules in 
place of the User entity in certain circumstances.

### VC Needs

VCNeed entities are used to list volunteer, giving, or other needs, and are 
used along with VCActions to create volunteer and donor applications.

The entities have statuses (VCNeedStatus entites), which in turn have allowed 
VCActions. These are going to be configuration entities managed in a 
[module].vc_need_status.yml file. Each status also has a boolean "active" 
property which in turn sets a corresponding "active" property on the Need. The 
"active" property is mainly meant for listing purposes - listing which needs 
are active and requiring some further action.

### VC Resources

VCResource entities are most similar to VCContacts, but are different mainly in 
practice than in data model. Resources are used to create lists, but do not 
have contact information.

## Contributions

### Developers

Drupal.org

* <a href="https://www.drupal.org/u/laboratorymike">laboratory.mike</a>

### Icons

Icons made by <a href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>