<?php

namespace Drupal\vc_events;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of VC Event entities.
 *
 * @ingroup vc_events
 */
class VCEventListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('VC Event ID');
    $header['type'] = $this->t('Type');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\vc_events\Entity\VCEvent $entity */
    $row['id'] = $entity->id();
    $row['type'] = $entity->bundle();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.vc_event.edit_form',
      ['vc_event' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
