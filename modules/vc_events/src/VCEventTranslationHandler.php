<?php

namespace Drupal\vc_events;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for vc_event.
 */
class VCEventTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
