<?php

namespace Drupal\vc_events\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VCEventTypeForm.
 */
class VCEventTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $vc_event_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $vc_event_type->label(),
      '#description' => $this->t("Label for the VC Event type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $vc_event_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\vc_events\Entity\VCEventType::load',
      ],
      '#disabled' => !$vc_event_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $vc_event_type = $this->entity;
    $status = $vc_event_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label VC Event type.', [
          '%label' => $vc_event_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label VC Event type.', [
          '%label' => $vc_event_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($vc_event_type->toUrl('collection'));
  }

}
