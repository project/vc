<?php

namespace Drupal\vc_events\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting VC Event entities.
 *
 * @ingroup vc_events
 */
class VCEventDeleteForm extends ContentEntityDeleteForm {


}
