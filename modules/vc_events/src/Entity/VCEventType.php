<?php

namespace Drupal\vc_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the VC Event type entity.
 *
 * @ConfigEntityType(
 *   id = "vc_event_type",
 *   label = @Translation("VC Event type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vc_events\VCEventTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\vc_events\Form\VCEventTypeForm",
 *       "edit" = "Drupal\vc_events\Form\VCEventTypeForm",
 *       "delete" = "Drupal\vc_events\Form\VCEventTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vc_events\VCEventTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "vc_event_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "vc_event",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vc_event_type/{vc_event_type}",
 *     "add-form" = "/admin/structure/vc_event_type/add",
 *     "edit-form" = "/admin/structure/vc_event_type/{vc_event_type}/edit",
 *     "delete-form" = "/admin/structure/vc_event_type/{vc_event_type}/delete",
 *     "collection" = "/admin/structure/vc_event_type"
 *   }
 * )
 */
class VCEventType extends ConfigEntityBundleBase implements VCEventTypeInterface {

  /**
   * The VC Event type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The VC Event type label.
   *
   * @var string
   */
  protected $label;

}
