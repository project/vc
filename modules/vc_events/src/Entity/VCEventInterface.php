<?php

namespace Drupal\vc_events\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VC Event entities.
 *
 * @ingroup vc_events
 */
interface VCEventInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the VC Event name.
   *
   * @return string
   *   Name of the VC Event.
   */
  public function getName();

  /**
   * Sets the VC Event name.
   *
   * @param string $name
   *   The VC Event name.
   *
   * @return \Drupal\vc_events\Entity\VCEventInterface
   *   The called VC Event entity.
   */
  public function setName($name);

  /**
   * Gets the VC Event creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VC Event.
   */
  public function getCreatedTime();

  /**
   * Sets the VC Event creation timestamp.
   *
   * @param int $timestamp
   *   The VC Event creation timestamp.
   *
   * @return \Drupal\vc_events\Entity\VCEventInterface
   *   The called VC Event entity.
   */
  public function setCreatedTime($timestamp);

}
