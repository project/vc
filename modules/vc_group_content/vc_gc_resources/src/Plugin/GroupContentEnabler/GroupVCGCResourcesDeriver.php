<?php

namespace Drupal\vc_gc_resources\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\vc_resources\Entity\VCResourceType;

class GroupVCGCResourcesDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (VCResourceType::loadMultiple() as $name => $vc_resource_type) {
      $label = $vc_resource_type->label();

      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Group resource (@type)', ['@type' => $label]),
        'description' => t('Adds %type content to groups both publicly and privately.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
