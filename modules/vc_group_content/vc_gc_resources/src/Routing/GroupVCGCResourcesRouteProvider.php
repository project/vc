<?php

namespace Drupal\vc_gc_resources\Routing;

use Drupal\vc_resources\Entity\VCResourceType;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for group_vc_gc_resources group content.
 */
class GroupVCGCResourcesRouteProvider {

  /**
   * Provides the shared collection route for group vc_resource_type plugins.
   */
  public function getRoutes() {
    $routes = $plugin_ids = $permissions_add = $permissions_create = [];

    foreach (VCResourceType::loadMultiple() as $name => $vc_resource_type) {
      $plugin_id = "group_vc_gc_resources:$name";

      $plugin_ids[] = $plugin_id;
      $permissions_add[] = "create $plugin_id content";
      $permissions_create[] = "create $plugin_id entity";
    }

    // If there are no vc_resource types yet, we cannot have any plugin IDs and should
    // therefore exit early because we cannot have any routes for them either.
    if (empty($plugin_ids)) {
      return $routes;
    }

    $routes['entity.group_content.group_vc_gc_resources_relate_page'] = new Route('group/{group}/vc_resource/add');
    $routes['entity.group_content.group_vc_gc_resources_relate_page']
      ->setDefaults([
        '_title' => 'Relate Resources',
        '_controller' => '\Drupal\vc_gc_resources\Controller\GroupVCGCResourcesController::addPage',
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_add))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    $routes['entity.group_content.group_vc_gc_resources_add_page'] = new Route('group/{group}/vc_resource/create');
    $routes['entity.group_content.group_vc_gc_resources_add_page']
      ->setDefaults([
        '_title' => 'Create Resource',
        '_controller' => '\Drupal\vc_gc_resources\Controller\GroupVCGCResourcesController::addPage',
        'create_mode' => TRUE,
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_create))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    return $routes;
  }

}
