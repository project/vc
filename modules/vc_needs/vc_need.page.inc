<?php

/**
 * @file
 * Contains vc_need.page.inc.
 *
 * Page callback for VCNeed entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for VCNeed templates.
 *
 * Default template: vc_need.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_vc_need(array &$variables) {
  // Fetch VCNeed Entity Object.
  $vc_need = $variables['elements']['#vc_need'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
