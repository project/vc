<?php

namespace Drupal\vc_needs;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\vc_needs\Entity\VCNeedType;


/**
 * Provides dynamic permissions for VCNeed of different types.
 *
 * @ingroup vc_needs
 *
 */
class VCNeedPermissions{

  use StringTranslationTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The VCNeedType by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];

    foreach (VCNeedType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\vc_needs\Entity\VCNeedType $type
   *   The VCNeed type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(VCNeedType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "$type_id create vc_need entities" => [
        'title' => $this->t('Create new %type_name entities', $type_params),
      ],
      "$type_id edit own vc_need entities" => [
        'title' => $this->t('Edit own %type_name entities', $type_params),
      ],
      "$type_id edit any vc_need entities" => [
        'title' => $this->t('Edit any %type_name entities', $type_params),
      ],
      "$type_id delete own vc_need entities" => [
        'title' => $this->t('Delete own %type_name entities', $type_params),
      ],
      "$type_id delete any vc_need entities" => [
        'title' => $this->t('Delete any %type_name entities', $type_params),
      ],
      "$type_id view vc_need revisions" => [
        'title' => $this->t('View %type_name revisions', $type_params),
        'description' => t('To view a revision, you also need permission to view the entity item.'),
      ],
      "$type_id revert vc_need revisions" => [
        'title' => $this->t('Revert %type_name revisions', $type_params),
        'description' => t('To revert a revision, you also need permission to edit the entity item.'),
      ],
      "$type_id delete vc_need revisions" => [
        'title' => $this->t('Delete %type_name revisions', $type_params),
        'description' => $this->t('To delete a revision, you also need permission to delete the entity item.'),
      ],
    ];
  }

}
