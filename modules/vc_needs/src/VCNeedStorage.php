<?php

namespace Drupal\vc_needs;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\vc_needs\Entity\VCNeedInterface;

/**
 * Defines the storage handler class for VCNeed entities.
 *
 * This extends the base storage class, adding required special handling for
 * VCNeed entities.
 *
 * @ingroup vc_needs
 */
class VCNeedStorage extends SqlContentEntityStorage implements VCNeedStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(VCNeedInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {vc_need_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {vc_need_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(VCNeedInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {vc_need_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('vc_need_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
