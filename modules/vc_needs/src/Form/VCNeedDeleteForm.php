<?php

namespace Drupal\vc_needs\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting VCNeed entities.
 *
 * @ingroup vc_needs
 */
class VCNeedDeleteForm extends ContentEntityDeleteForm {


}
