<?php

namespace Drupal\vc_needs\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the VCNeed type entity.
 *
 * @ConfigEntityType(
 *   id = "vc_need_type",
 *   label = @Translation("VCNeed type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vc_needs\VCNeedTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\vc_needs\Form\VCNeedTypeForm",
 *       "edit" = "Drupal\vc_needs\Form\VCNeedTypeForm",
 *       "delete" = "Drupal\vc_needs\Form\VCNeedTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vc_needs\VCNeedTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "vc_need_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "vc_need",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vc_need_type/{vc_need_type}",
 *     "add-form" = "/admin/structure/vc_need_type/add",
 *     "edit-form" = "/admin/structure/vc_need_type/{vc_need_type}/edit",
 *     "delete-form" = "/admin/structure/vc_need_type/{vc_need_type}/delete",
 *     "collection" = "/admin/structure/vc_need_type"
 *   }
 * )
 */
class VCNeedType extends ConfigEntityBundleBase implements VCNeedTypeInterface {

  /**
   * The VCNeed type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The VCNeed type label.
   *
   * @var string
   */
  protected $label;

}
