<?php

namespace Drupal\vc_needs\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the VCNeed entity.
 *
 * @ingroup vc_needs
 *
 * @ContentEntityType(
 *   id = "vc_need",
 *   label = @Translation("VCNeed"),
 *   bundle_label = @Translation("VCNeed type"),
 *   handlers = {
 *     "storage" = "Drupal\vc_needs\VCNeedStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vc_needs\VCNeedListBuilder",
 *     "views_data" = "Drupal\vc_needs\Entity\VCNeedViewsData",
 *     "translation" = "Drupal\vc_needs\VCNeedTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\vc_needs\Form\VCNeedForm",
 *       "add" = "Drupal\vc_needs\Form\VCNeedForm",
 *       "edit" = "Drupal\vc_needs\Form\VCNeedForm",
 *       "delete" = "Drupal\vc_needs\Form\VCNeedDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vc_needs\VCNeedHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\vc_needs\VCNeedAccessControlHandler",
 *   },
 *   base_table = "vc_need",
 *   data_table = "vc_need_field_data",
 *   revision_table = "vc_need_revision",
 *   revision_data_table = "vc_need_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer vc_need entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/vc_need/{vc_need}",
 *     "add-page" = "/vc_need/add",
 *     "add-form" = "/vc_need/add/{vc_need_type}",
 *     "edit-form" = "/vc_need/{vc_need}/edit",
 *     "delete-form" = "/vc_need/{vc_need}/delete",
 *     "version-history" = "/vc_need/{vc_need}/revisions",
 *     "revision" = "/vc_need/{vc_need}/revisions/{vc_need_revision}/view",
 *     "revision_revert" = "/vc_need/{vc_need}/revisions/{vc_need_revision}/revert",
 *     "revision_delete" = "/vc_need/{vc_need}/revisions/{vc_need_revision}/delete",
 *     "translation_revert" = "/vc_need/{vc_need}/revisions/{vc_need_revision}/revert/{langcode}",
 *     "collection" = "/vc_need",
 *   },
 *   bundle_entity_type = "vc_need_type",
 *   field_ui_base_route = "entity.vc_need_type.edit_form"
 * )
 */
class VCNeed extends EditorialContentEntityBase implements VCNeedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the vc_need owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  public static function getNeedStatusesAsSelectOptions($bundle = FALSE) {
    $plugin_manager = \Drupal::service('plugin.manager.vc_need_status');
    $definitions = $plugin_manager->getDefinitions();
    $options = [];
    foreach ($definitions as $plugin_id => $definition) {
      // Bundle not set - list all
      // Bundle set - list compatible (matching bundle name or all_bundles = TRUE.
      if (!$bundle ||
        ($bundle && isset($definition['bundles']) && in_array($bundle, $definition['bundles'])) ||
        (isset($definition['all_bundles']) && $definition['all_bundles'])
      ) {
        $options[$plugin_id] = $definition['label'];
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the VCNeed entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the VCNeed entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active Need'))
      ->setRevisionable(TRUE)
      ->setDescription(t('A boolean indicating whether the Need is active for volunteer purposes.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['need_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Need Status'))
      ->setDescription(t('The current status of the need'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'allowed_values' => self::getNeedStatusesAsSelectOptions(),
        'text_processing' => 0,
      ])
      ->setDefaultValue('active')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the VCNeed is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
