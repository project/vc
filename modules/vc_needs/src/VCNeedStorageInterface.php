<?php

namespace Drupal\vc_needs;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\vc_needs\Entity\VCNeedInterface;

/**
 * Defines the storage handler class for VCNeed entities.
 *
 * This extends the base storage class, adding required special handling for
 * VCNeed entities.
 *
 * @ingroup vc_needs
 */
interface VCNeedStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of VCNeed revision IDs for a specific VCNeed.
   *
   * @param \Drupal\vc_needs\Entity\VCNeedInterface $entity
   *   The VCNeed entity.
   *
   * @return int[]
   *   VCNeed revision IDs (in ascending order).
   */
  public function revisionIds(VCNeedInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as VCNeed author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   VCNeed revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\vc_needs\Entity\VCNeedInterface $entity
   *   The VCNeed entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(VCNeedInterface $entity);

  /**
   * Unsets the language for all VCNeed with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
