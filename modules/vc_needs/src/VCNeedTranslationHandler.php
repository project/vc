<?php

namespace Drupal\vc_needs;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for vc_need.
 */
class VCNeedTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
