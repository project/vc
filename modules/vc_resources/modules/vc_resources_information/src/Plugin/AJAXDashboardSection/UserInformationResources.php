<?php

namespace Drupal\vc_resources_information\Plugin\AJAXDashboardSection;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardSectionBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;

/**
 * Class UserResources
 * @package Drupal\vc_contacts_volunteers\Plugin\AJAXDashboardSection
 *
 * @AJAXDashboardSection (
 *   id = "vc_user_info_resources",
 *   label = "User Resources"
 * )
 *
 */
class UserInformationResources extends AJAXDashboardSectionBase {

  /**
   * @param $form
   * @param FormStateInterface $form_state
   * @param null $context
   * @param null $params
   * @return array|AjaxResponse|mixed
   */
  public static function buildDashboard($form, FormStateInterface $form_state, $context = NULL, $params = NULL) {
    if ($trigger = $form_state->getTriggeringElement()) {
      $add = [
        '#type' => 'container',
        '#prefix' => '<div id="edit-dashboard" class="dashboard-wrapper">',
        '#suffix' => '</div>',
      ];
      switch ($trigger['#id']) {
        case 'vc_resources_manage_information_resources':
          $add['value'] = self::buildManagedResources($trigger['#user']);
          break;
      }
      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand('#edit-dashboard', $add));
      return $response;
    }
    else {
      if (isset($context['user'])) {
        return self::buildManagedResources($context['user']);
      }
      else {
        return [];
      }
    }

  }

  /**
   * @param $form
   * @param FormStateInterface $form_state
   * @param array $context
   * @param array $params
   * @return array|mixed
   */
  public static function buildSection($form, FormStateInterface $form_state, array $context = [], array $params = []) {
    if (isset($context['user'])) {
      $user = $context['user'];
      $section = [
        '#type' => 'container',
        '#cache' => [
          'contexts' => ['user', 'user.permissions']
        ]
      ];

      $u = \Drupal\user\Entity\User::load($user);
      $section['vc_resources_managed_info_resources'] = [
        '#type' => 'button',
        '#value' => t('Information Resources'),
        '#id' => 'vc_resources_manage_information_resources',
        '#user' => $user,
        '#access' => $u->hasPermission('create vc_resource entities') || $u->hasPermission('information_resource create vc_resource entities') ,
        '#ajax' => [
          'callback' => get_called_class() . '::buildDashboard',
          'event' => 'click',
          'wrapper' => 'edit-dashboard',
        ],
      ];

      return $section;
    }
  }

  public static function buildManagedResources($user) {
    $view = Views::getView('vc_resources_manage_information_resources');
    $view->setArguments([$user]);
    return $view->preview('default');
  }

}