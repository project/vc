<?php

namespace Drupal\vc_resources\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the VCResource type entity.
 *
 * @ConfigEntityType(
 *   id = "vc_resource_type",
 *   label = @Translation("VCResource type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vc_resources\VCResourceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\vc_resources\Form\VCResourceTypeForm",
 *       "edit" = "Drupal\vc_resources\Form\VCResourceTypeForm",
 *       "delete" = "Drupal\vc_resources\Form\VCResourceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vc_resources\VCResourceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "vc_resource_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "vc_resource",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vc_resource_type/{vc_resource_type}",
 *     "add-form" = "/admin/structure/vc_resource_type/add",
 *     "edit-form" = "/admin/structure/vc_resource_type/{vc_resource_type}/edit",
 *     "delete-form" = "/admin/structure/vc_resource_type/{vc_resource_type}/delete",
 *     "collection" = "/admin/structure/vc_resource_type"
 *   }
 * )
 */
class VCResourceType extends ConfigEntityBundleBase implements VCResourceTypeInterface {

  /**
   * The VCResource type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The VCResource type label.
   *
   * @var string
   */
  protected $label;

}
