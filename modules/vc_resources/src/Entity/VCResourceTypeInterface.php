<?php

namespace Drupal\vc_resources\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining VCResource type entities.
 */
interface VCResourceTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
