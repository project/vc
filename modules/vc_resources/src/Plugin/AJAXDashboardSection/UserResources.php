<?php

namespace Drupal\vc_resources\Plugin\AJAXDashboardSection;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardSectionBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;

/**
 * Class UserResources
 * @package Drupal\vc_contacts_volunteers\Plugin\AJAXDashboardSection
 *
 * @AJAXDashboardSection (
 *   id = "vc_user_resources",
 *   label = "User Resources"
 * )
 *
 */
class UserResources extends AJAXDashboardSectionBase {

  /**
   * @param $form
   * @param FormStateInterface $form_state
   * @param null $context
   * @param null $params
   * @return array|AjaxResponse|mixed
   */
  public static function buildDashboard($form, FormStateInterface $form_state, $context = NULL, $params = NULL) {
    // This is never called programmatically, but might be called if it is the first section.
    return [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('Resources')
    ];
  }

  /**
   * @param $form
   * @param FormStateInterface $form_state
   * @param array $context
   * @param array $params
   * @return array|mixed
   */
  public static function buildSection($form, FormStateInterface $form_state, array $context = [], array $params = []) {
    if (isset($context['user'])) {
      $section = [
        '#type' => 'container',
        '#cache' => [
          'contexts' => ['user', 'user.permissions']
        ]
      ];

      $section['vc_resources_header'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => t('Resources'),
        '#attributes' => [
          'class' => ['section-resources']
        ]
      ];

      return $section;
    }
  }

}