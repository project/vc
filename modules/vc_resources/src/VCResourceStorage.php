<?php

namespace Drupal\vc_resources;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\vc_resources\Entity\VCResourceInterface;

/**
 * Defines the storage handler class for VCResource entities.
 *
 * This extends the base storage class, adding required special handling for
 * VCResource entities.
 *
 * @ingroup vc_resources
 */
class VCResourceStorage extends SqlContentEntityStorage implements VCResourceStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(VCResourceInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {vc_resource_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {vc_resource_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(VCResourceInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {vc_resource_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('vc_resource_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
