<?php

/**
 * @file
 * Contains vc_resource.page.inc.
 *
 * Page callback for VCResource entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for VCResource templates.
 *
 * Default template: vc_resource.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_vc_resource(array &$variables) {
  // Fetch VCResource Entity Object.
  $vc_resource = $variables['elements']['#vc_resource'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
