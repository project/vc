<?php

namespace Drupal\vc_contacts\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\vc_contacts\Entity\VCContactInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VCContactController.
 *
 *  Returns responses for VCContact routes.
 */
class VCContactController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a VCContact revision.
   *
   * @param int $vc_contact_revision
   *   The VCContact revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($vc_contact_revision) {
    $vc_contact = $this->entityTypeManager()->getStorage('vc_contact')
      ->loadRevision($vc_contact_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('vc_contact');

    return $view_builder->view($vc_contact);
  }

  /**
   * Page title callback for a VCContact revision.
   *
   * @param int $vc_contact_revision
   *   The VCContact revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($vc_contact_revision) {
    $vc_contact = $this->entityTypeManager()->getStorage('vc_contact')
      ->loadRevision($vc_contact_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $vc_contact->label(),
      '%date' => $this->dateFormatter->format($vc_contact->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a VCContact.
   *
   * @param \Drupal\vc_contacts\Entity\VCContactInterface $vc_contact
   *   A VCContact object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(VCContactInterface $vc_contact) {
    $account = $this->currentUser();
    $vc_contact_storage = $this->entityTypeManager()->getStorage('vc_contact');

    $langcode = $vc_contact->language()->getId();
    $langname = $vc_contact->language()->getName();
    $languages = $vc_contact->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $vc_contact->label()]) : $this->t('Revisions for %title', ['%title' => $vc_contact->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all vc_contact revisions") || $account->hasPermission('administer vc_contact entities')));
    $delete_permission = (($account->hasPermission("delete all vc_contact revisions") || $account->hasPermission('administer vc_contact entities')));

    $rows = [];

    $vids = $vc_contact_storage->revisionIds($vc_contact);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\vc_contacts\VCContactInterface $revision */
      $revision = $vc_contact_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $vc_contact->getRevisionId()) {
          $link = $this->l($date, new Url('entity.vc_contact.revision', [
            'vc_contact' => $vc_contact->id(),
            'vc_contact_revision' => $vid,
          ]));
        }
        else {
          $link = $vc_contact->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.vc_contact.translation_revert', [
                'vc_contact' => $vc_contact->id(),
                'vc_contact_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.vc_contact.revision_revert', [
                'vc_contact' => $vc_contact->id(),
                'vc_contact_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.vc_contact.revision_delete', [
                'vc_contact' => $vc_contact->id(),
                'vc_contact_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['vc_contact_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
