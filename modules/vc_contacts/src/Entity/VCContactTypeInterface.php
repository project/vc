<?php

namespace Drupal\vc_contacts\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining VCContact type entities.
 */
interface VCContactTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
