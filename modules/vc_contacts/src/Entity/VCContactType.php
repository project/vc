<?php

namespace Drupal\vc_contacts\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the VCContact type entity.
 *
 * @ConfigEntityType(
 *   id = "vc_contact_type",
 *   label = @Translation("VCContact type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vc_contacts\VCContactTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\vc_contacts\Form\VCContactTypeForm",
 *       "edit" = "Drupal\vc_contacts\Form\VCContactTypeForm",
 *       "delete" = "Drupal\vc_contacts\Form\VCContactTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vc_contacts\VCContactTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "vc_contact_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "vc_contact",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vc_contact_type/{vc_contact_type}",
 *     "add-form" = "/admin/structure/vc_contact_type/add",
 *     "edit-form" = "/admin/structure/vc_contact_type/{vc_contact_type}/edit",
 *     "delete-form" = "/admin/structure/vc_contact_type/{vc_contact_type}/delete",
 *     "collection" = "/admin/structure/vc_contact_type"
 *   }
 * )
 */
class VCContactType extends ConfigEntityBundleBase implements VCContactTypeInterface {

  /**
   * The VCContact type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The VCContact type label.
   *
   * @var string
   */
  protected $label;

}
