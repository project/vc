<?php

namespace Drupal\vc_contacts\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VCContact entities.
 *
 * @ingroup vc_contacts
 */
interface VCContactInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the VCContact name.
   *
   * @return string
   *   Name of the VCContact.
   */
  public function getName();

  /**
   * Sets the VCContact name.
   *
   * @param string $name
   *   The VCContact name.
   *
   * @return \Drupal\vc_contacts\Entity\VCContactInterface
   *   The called VCContact entity.
   */
  public function setName($name);

  /**
   * Gets the VCContact creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VCContact.
   */
  public function getCreatedTime();

  /**
   * Sets the VCContact creation timestamp.
   *
   * @param int $timestamp
   *   The VCContact creation timestamp.
   *
   * @return \Drupal\vc_contacts\Entity\VCContactInterface
   *   The called VCContact entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the VCContact revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the VCContact revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\vc_contacts\Entity\VCContactInterface
   *   The called VCContact entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the VCContact revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the VCContact revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\vc_contacts\Entity\VCContactInterface
   *   The called VCContact entity.
   */
  public function setRevisionUserId($uid);

}
