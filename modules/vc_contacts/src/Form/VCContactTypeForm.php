<?php

namespace Drupal\vc_contacts\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VCContactTypeForm.
 */
class VCContactTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $vc_contact_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $vc_contact_type->label(),
      '#description' => $this->t("Label for the VCContact type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $vc_contact_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\vc_contacts\Entity\VCContactType::load',
      ],
      '#disabled' => !$vc_contact_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $vc_contact_type = $this->entity;
    $status = $vc_contact_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label VCContact type.', [
          '%label' => $vc_contact_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label VCContact type.', [
          '%label' => $vc_contact_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($vc_contact_type->toUrl('collection'));
  }

}
