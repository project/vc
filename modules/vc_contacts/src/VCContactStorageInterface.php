<?php

namespace Drupal\vc_contacts;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\vc_contacts\Entity\VCContactInterface;

/**
 * Defines the storage handler class for VCContact entities.
 *
 * This extends the base storage class, adding required special handling for
 * VCContact entities.
 *
 * @ingroup vc_contacts
 */
interface VCContactStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of VCContact revision IDs for a specific VCContact.
   *
   * @param \Drupal\vc_contacts\Entity\VCContactInterface $entity
   *   The VCContact entity.
   *
   * @return int[]
   *   VCContact revision IDs (in ascending order).
   */
  public function revisionIds(VCContactInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as VCContact author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   VCContact revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\vc_contacts\Entity\VCContactInterface $entity
   *   The VCContact entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(VCContactInterface $entity);

  /**
   * Unsets the language for all VCContact with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
