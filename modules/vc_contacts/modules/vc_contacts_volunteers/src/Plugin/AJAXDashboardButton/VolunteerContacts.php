<?php

namespace Drupal\vc_contacts_volunteers\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\vc_contacts\Entity\VCContact;

/**
 * Class VolunteerContacts
 * @package Drupal\vc_contacts_volunteers\Plugin\AJAXDashboardButton
 *
 * @AJAXDashboardButton (
 *   id = "vc_volunteer_contact",
 *   label = "Volunteer Contacts"
 * )
 *
 */
class VolunteerContacts extends AJAXDashboardButtonBase {


  /**
   * @param $uid
   * @return \Drupal\Core\Entity\EntityInterface[]|VCContact[]
   */
  public static function getProfile($uid) {
    $query = \Drupal::entityQuery('vc_contact');
    $query->condition('type', 'volunteer');
    $query->condition('field_associate_user', $uid);
    return VCContact::loadMultiple($query->execute());
  }

  /**
   * {@inheritdoc}
   */
  public static function access(array $params = [], array $button_data = [], AccountInterface $account = NULL) {
    $account = $account ? $account : \Drupal::currentUser();
    return $account->hasPermission('volunteer create vc_contact entities') || $account->hasPermission('add vc_contact entities');
  }

  /**
   * {@inheritdoc}
   */
  public static function getButtonStatus($params = [], $button_data = []) {
    return empty(self::getProfile($params['user'])) ? 'incomplete' : 'complete';
  }

  /**
   * {@inheritdoc}
   */
  public static function getButtonLabel($params = [], $button_data = []) {
    if (isset($button_data['label'])) {
      return $button_data['label'];
    }
    else {
      return t('Button');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent($params = [], $button_data = []) {
    $profiles = self::getProfile($params['user']);
    $user = $params['user'];
    if (!empty($profiles)) {
      $profile = array_shift($profiles);
      /** @var $profile \Drupal\vc_contacts\Entity\VCContact */
      return [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => t('Profile'),
        ],
        'edit' => Link::createFromRoute('Edit Profile',
          'entity.vc_contact.edit_form',
          ['vc_contact' => $profile->id()],
          ['attributes' => ['class' => ['btn btn-info btn-sm']],
            'query' => ['destination' => 'user/'.$user, 'associate-user' => $user]])->toRenderable(),
        'profile' => \Drupal::entityTypeManager()->getViewBuilder('vc_contact')->view($profile)
      ];
    }
    else {
      return [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => t('Create your profile to get started'),
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => t('Your profile is used for all volunteering and contact information. Additional information can be added in later sections.'),
        ],
        'button' => Link::createFromRoute('+ Create a Profile',
          'entity.vc_contact.add_form',
          ['vc_contact_type' => 'volunteer'],
          ['attributes' => ['class' => ['btn btn-sm btn-success']],
            'query' => ['destination' => 'user/'.$user, 'associate-user' => $user]])->toRenderable()
      ];
    }
  }

  public static function buildPrimaryContact($user) {
    $query = \Drupal::entityQuery('vc_contact');
    $query->condition('type', 'volunteer');
    $query->condition('field_associate_user', $user);
    $profiles = VCContact::loadMultiple($query->execute());

    if (!empty($profiles)) {
      $profile = array_shift($profiles);
      /** @var $profile \Drupal\vc_contacts\Entity\VCContact */
      return [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => t('Profile'),
        ],
        'edit' => Link::createFromRoute('Edit Profile',
          'entity.vc_contact.edit_form',
          ['vc_contact' => $profile->id()],
          ['attributes' => ['class' => ['btn btn-info btn-sm']],
            'query' => ['destination' => 'user/'.$user, 'associate-user' => $user]])->toRenderable(),
        'profile' => \Drupal::entityTypeManager()->getViewBuilder('vc_contact')->view($profile)
      ];
    }
    else {
      return [
        'header' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => t('Create your profile to get started'),
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => t('Your profile is used for all volunteering and contact information. Additional information can be added in later sections.'),
        ],
        'button' => Link::createFromRoute('+ Create a Profile',
          'entity.vc_contact.add_form',
          ['vc_contact_type' => 'volunteer'],
          ['attributes' => ['class' => ['btn btn-sm btn-success']],
            'query' => ['destination' => 'user/'.$user, 'associate-user' => $user]])->toRenderable()
      ];
    }
  }

}