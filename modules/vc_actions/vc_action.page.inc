<?php

/**
 * @file
 * Contains vc_action.page.inc.
 *
 * Page callback for VCAction entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for VCAction templates.
 *
 * Default template: vc_action.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_vc_action(array &$variables) {
  // Fetch VCAction Entity Object.
  $vc_action = $variables['elements']['#vc_action'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
