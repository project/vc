<?php

namespace Drupal\vc_actions\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting VCAction entities.
 *
 * @ingroup vc_actions
 */
class VCActionDeleteForm extends ContentEntityDeleteForm {


}
