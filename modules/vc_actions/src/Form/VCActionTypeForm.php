<?php

namespace Drupal\vc_actions\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VCActionTypeForm.
 */
class VCActionTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $vc_action_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $vc_action_type->label(),
      '#description' => $this->t("Label for the VCAction type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $vc_action_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\vc_actions\Entity\VCActionType::load',
      ],
      '#disabled' => !$vc_action_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $vc_action_type = $this->entity;
    $status = $vc_action_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label VCAction type.', [
          '%label' => $vc_action_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label VCAction type.', [
          '%label' => $vc_action_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($vc_action_type->toUrl('collection'));
  }

}
