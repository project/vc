<?php

namespace Drupal\vc_actions;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for vc_action.
 */
class VCActionTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
