<?php

namespace Drupal\vc_actions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the VCAction type entity.
 *
 * @ConfigEntityType(
 *   id = "vc_action_type",
 *   label = @Translation("VCAction type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\vc_actions\VCActionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\vc_actions\Form\VCActionTypeForm",
 *       "edit" = "Drupal\vc_actions\Form\VCActionTypeForm",
 *       "delete" = "Drupal\vc_actions\Form\VCActionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vc_actions\VCActionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "vc_action_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "vc_action",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vc_action_type/{vc_action_type}",
 *     "add-form" = "/admin/structure/vc_action_type/add",
 *     "edit-form" = "/admin/structure/vc_action_type/{vc_action_type}/edit",
 *     "delete-form" = "/admin/structure/vc_action_type/{vc_action_type}/delete",
 *     "collection" = "/admin/structure/vc_action_type"
 *   }
 * )
 */
class VCActionType extends ConfigEntityBundleBase implements VCActionTypeInterface {

  /**
   * The VCAction type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The VCAction type label.
   *
   * @var string
   */
  protected $label;

}
