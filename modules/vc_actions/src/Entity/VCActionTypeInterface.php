<?php

namespace Drupal\vc_actions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining VCAction type entities.
 */
interface VCActionTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
