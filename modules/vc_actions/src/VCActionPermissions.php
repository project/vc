<?php

namespace Drupal\vc_actions;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\vc_actions\Entity\VCActionType;

/**
 * Provides dynamic permissions for VCAction of different types.
 *
 * @ingroup vc_actions
 *
 */
class VCActionPermissions{

  use StringTranslationTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The VCActionType by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];

    foreach (VCActionType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\vc_actions\Entity\VCActionType $type
   *   The VCAction type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(VCActionType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "$type_id create vc_action entities" => [
        'title' => $this->t('Create new %type_name entities', $type_params),
      ],
      "$type_id edit own vc_action entities" => [
        'title' => $this->t('Edit own %type_name entities', $type_params),
      ],
      "$type_id edit any vc_action entities" => [
        'title' => $this->t('Edit any %type_name entities', $type_params),
      ],
      "$type_id delete own vc_action entities" => [
        'title' => $this->t('Delete own %type_name entities', $type_params),
      ],
      "$type_id delete any vc_action entities" => [
        'title' => $this->t('Delete any %type_name entities', $type_params),
      ],
    ];
  }

}
