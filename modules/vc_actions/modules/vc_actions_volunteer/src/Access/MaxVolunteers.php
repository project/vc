<?php

namespace Drupal\vc_actions_volunteer\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\vc_actions_volunteer\VCActionsVolunteerTrait;
use Symfony\Component\Routing\Route;

/**
 * Class NewVolunteer
 * @package Drupal\vc_actions_volunteer\Access
 */
class MaxVolunteers implements AccessInterface {

  use VCActionsVolunteerTrait;

  /**
   * Check whether the person has volunteered already.
   *
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return AccessResult|\Drupal\Core\Access\AccessResultAllowed
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $vc_need = $route_match->getParameter('vc_need');
    $max = $this->checkMaxVolunteers($vc_need, $account);
    return $max ? AccessResult::allowedIfHasPermission($account,'vc_actions_volunteer volunteer for need') : AccessResult::forbidden();
  }
}
