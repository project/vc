<?php

namespace Drupal\vc_actions_volunteer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\vc_actions\Entity\VCAction;
use Drupal\vc_needs\Entity\VCNeed;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class VCActionsVolunteerController
 * @package Drupal\vc_actions_volunteer\Controller
 */
class VCActionsVolunteerController extends ControllerBase {

  /**
   * Redirect to the previous page.
   */
  public function redirectToPrevious() {
    // Return to where we came from
    $server = Request::createFromGlobals()->server;
    $return = $server->get('HTTP_REFERER');
    $http = $server->get('HTTPS') ? 'https://' : 'http://';
    $domain = $http . $server->get('SERVER_NAME');
    $port = ':' . $server->get('SERVER_PORT');
    $return = str_replace($domain, '', $return);
    $return = str_replace($port, '', $return);
    $return = $return ? $return : '/';
    $url = Url::fromUserInput($return);

    $response = new RedirectResponse($url->toString());
    return $response;
  }

  /**
   * @param VCNeed $vc_need
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function volunteer(VCNeed $vc_need) {

    $volunteer = \Drupal::currentUser();
    $volunteer_name = \Drupal::currentUser()->getDisplayName();

    $profile_query = \Drupal::entityQuery('vc_contact');
    $profile_query->condition('type', 'volunteer');
    $profile_query->condition('field_associate_user', $volunteer->id());
    $result = $profile_query->execute();
    $profile = array_shift($result);
    if ($profile) {
      $action_data = [
        'name' => t('%volunteer volunteering for %need', ['%volunteer' => $volunteer_name, '%need' => $vc_need->label()]),
        'type' => 'volunteer_action',
        'field_link_to_profile' => $profile,
        'field_volunteer' => $volunteer,
        'field_need' => $vc_need->id()
      ];
      $action_data['name'] = strip_tags($action_data['name']->render());

      $confirm = $vc_need->get('field_require_confirmation')->getValue();
      $confirm = isset($confirm[0]['value']) ? $confirm[0]['value'] : TRUE;
      $action_data['field_action_status'] = $confirm ? 'request' : 'commit';

      $action = VCAction::create($action_data);
      $action->save();
    }
    else {
      \Drupal::messenger()->addWarning('We were unable to detect your profile. Please create a volunteer profile on your account page.');
    }


    return $this->redirectToPrevious();
  }

  /**
   * @param VCNeed $vc_need
   * @param VCAction $vc_action
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function approveRequest(VCNeed $vc_need, VCAction $vc_action) {
    $vc_action->set('field_action_status','commit')->save();
    return $this->redirectToPrevious();
  }

  /**
   * @param VCNeed $vc_need
   * @param VCAction $vc_action
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function rejectRequest(VCNeed $vc_need, VCAction $vc_action) {
    $vc_action->set('field_action_status','cancel')->save();
    return $this->redirectToPrevious();
  }

  /**
   * @param VCAction $vc_action
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function markConfirm(VCAction $vc_action) {
    $vc_action->set('field_action_status','confirm')->save();
    return $this->redirectToPrevious();
  }

  /**
   * @param VCNeed $vc_need
   * @param VCAction $vc_action
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function markComplete(VCNeed $vc_need, VCAction $vc_action) {
    $vc_action->set('field_action_status','complete')->save();
    return $this->redirectToPrevious();
  }

  /**
   * @param VCAction $vc_action
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cancelByVolunteer(VCAction $vc_action) {
    $vc_action->set('field_action_status','cancel')->save();
    return $this->redirectToPrevious();
  }

  /**
   * @param VCNeed $vc_need
   * @param VCAction $vc_action
   * @return RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cancelByNeedOwner(VCNeed $vc_need, VCAction $vc_action) {
    $vc_action->set('field_action_status','cancel_volunteer')->save();
    return $this->redirectToPrevious();
  }

}
