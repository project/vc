<?php

/**
 * @file
 * Views area plugin info for views_add_button.
 */

/**
 * Implements hook_views_data_alter().
 */
function vc_actions_volunteer_views_data_alter(array &$data) {
  $data['vc_need']['volunteer_for_need'] = [
    'title' => t('Volunteer for Need'),
    'help' => t('Provide a volunteer button, or explanation for why one cannot volunteer.'),
    'field' => ['id' => 'vc_actions_volunteer_for_need'],
  ];
}
